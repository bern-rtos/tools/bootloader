#![no_std]

pub use mcuboot_config_definition::*;

const NUMBER_OF_KEYS: usize = 1;

// Example for EC256 and tinycrypt
const SIG_KEY_ONE_LEN: usize = 91;
const SIG_KEY_ONE: [u8; SIG_KEY_ONE_LEN] = [
    0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2a, 0x86,
    0x48, 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08, 0x2a,
    0x86, 0x48, 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03,
    0x42, 0x00, 0x04, 0x7f, 0x3d, 0x97, 0xfb, 0x01,
    0xfe, 0x6b, 0xa0, 0x62, 0x85, 0x93, 0x04, 0x9a,
    0x9e, 0x39, 0x11, 0x39, 0x90, 0x4a, 0xb6, 0x20,
    0xd4, 0x27, 0x8b, 0xe2, 0xed, 0x4b, 0xee, 0x21,
    0x39, 0x5f, 0xde, 0xf4, 0x1f, 0x7a, 0xc2, 0x2b,
    0xc6, 0xfa, 0x30, 0xde, 0x81, 0x43, 0x83, 0xdf,
    0xab, 0x35, 0x2d, 0xc8, 0x44, 0x12, 0x67, 0x3c,
    0x91, 0xe1, 0x6b, 0xbd, 0xe0, 0x9a, 0x77, 0x8e,
    0xeb, 0x68, 0xbf
];

// Example for RSA with mbed-tls
//const SIG_KEY_ONE_LEN: usize = 270;
//const SIG_KEY_ONE: [u8; SIG_KEY_ONE_LEN] = [
//    0x30, 0x82, 0x01, 0x0a, 0x02, 0x82, 0x01, 0x01,
//    0x00, 0xbe, 0x6d, 0x29, 0x06, 0x58, 0xf9, 0x23,
//    0x09, 0x94, 0x14, 0x28, 0xa9, 0x7b, 0xf1, 0x6e,
//    0x2f, 0x24, 0xb1, 0x4c, 0x1d, 0xc4, 0x36, 0xac,
//    0x93, 0x80, 0x76, 0x5a, 0x12, 0xad, 0xe2, 0x23,
//    0x3e, 0x06, 0x37, 0xce, 0xc9, 0x64, 0x2a, 0x6f,
//    0x1f, 0xc6, 0xf8, 0x3c, 0xc6, 0x5b, 0x2e, 0xd4,
//    0x1e, 0xc0, 0x4a, 0xb4, 0xf7, 0xac, 0x59, 0x48,
//    0xb1, 0xf9, 0x0f, 0x09, 0x2e, 0xb1, 0x9d, 0xa9,
//    0x1e, 0x86, 0x04, 0x4f, 0x77, 0x8b, 0xbe, 0xd8,
//    0xf9, 0x33, 0x25, 0x26, 0x55, 0x72, 0x99, 0xd7,
//    0xac, 0xee, 0x76, 0x46, 0x4a, 0xdb, 0xc9, 0xdb,
//    0xea, 0x01, 0x26, 0xfd, 0x15, 0xc9, 0xd5, 0xcc,
//    0x3b, 0x82, 0xa8, 0x67, 0xb7, 0x80, 0x38, 0x99,
//    0xa1, 0x23, 0x76, 0x68, 0x85, 0x87, 0x8f, 0x02,
//    0x82, 0x68, 0x5c, 0x06, 0x1e, 0xf0, 0xd7, 0xcf,
//    0xe8, 0xea, 0x20, 0xee, 0x4e, 0x9b, 0x6b, 0xa7,
//    0xcd, 0x41, 0xfa, 0x4c, 0x41, 0x40, 0x7a, 0x5b,
//    0x2a, 0x6b, 0xc6, 0x30, 0x1a, 0x78, 0xcf, 0x38,
//    0xdb, 0xec, 0xcd, 0xce, 0x27, 0x85, 0xbf, 0xaf,
//    0x25, 0x6b, 0x32, 0x84, 0x91, 0x68, 0xf7, 0xaf,
//    0xaf, 0xd7, 0x26, 0x4e, 0xb4, 0x3e, 0x03, 0xe3,
//    0x66, 0xe0, 0x95, 0x70, 0xbf, 0x31, 0x18, 0x81,
//    0xa3, 0x25, 0xa1, 0x46, 0xbc, 0x9e, 0xf9, 0x57,
//    0xbf, 0xe6, 0xf4, 0xe8, 0x2b, 0x7a, 0xe1, 0x72,
//    0x76, 0x6a, 0xc0, 0xc8, 0xd7, 0xc3, 0xe3, 0xfc,
//    0xd1, 0x41, 0xca, 0x5b, 0x4a, 0x06, 0x0e, 0xe7,
//    0x69, 0xcb, 0x2f, 0x86, 0x71, 0x89, 0x4b, 0xfc,
//    0xe1, 0xc0, 0x71, 0x68, 0x7c, 0x69, 0xc5, 0xd1,
//    0x0f, 0xfa, 0xfe, 0x7a, 0x95, 0x6f, 0xfc, 0xa7,
//    0xa0, 0xc2, 0xa3, 0xf5, 0x20, 0xc1, 0x00, 0xc1,
//    0x70, 0xee, 0xb2, 0x84, 0xac, 0xef, 0xb4, 0x43,
//    0x36, 0x72, 0x92, 0xc3, 0xe6, 0x61, 0x1b, 0x3a,
//    0x31, 0x02, 0x03, 0x01, 0x00, 0x01,
//];

pub const GENERAL_CONFIG: GeneralConfig<NUMBER_OF_KEYS> = GeneralConfig {
//    encryption: Encryption::MbedTls(MbedTlsSignature::Rsa), // Example for RSA with mbed-tls
    encryption: Encryption::TinyCrypt(TinycryptSignature::EC256), // Example with EC256 and tinycrypt
//    encryption: Encryption::MbedTls(MbedTlsSignature::EC256), // Example with EC256 and tinycrypt
    keys: [BootUtilKey::new(&SIG_KEY_ONE, &SIG_KEY_ONE_LEN)],
    upgrade_mode: UpgradeMode::SwapMove,
    direct_xip: DirectXip::Disable,
    ram_load: RamLoad::Disable,
    validate_primary_slot: ValidatePrimarySlot::Enable,
    max_image_sectors: 128,
    number_of_images: 1,
};

pub const FLASH_CONFIG: SysflashConfig = SysflashConfig {
    device_internal_flash: 0,
    slot_does_not_exist: 255,
    flash_area_image_scratch: 255,
    flash_area_bootloader: 0,
    flash_area_image_primary: |v| match v {
        0 => 1,
        _ => 255,
    },
    flash_area_image_secondary: |v| match v {
        0 => 2,
        _ => 255,
    },
};

pub const MCUBOOT_CONFIG: McubootConfig<NUMBER_OF_KEYS> = McubootConfig {
    general_config: GENERAL_CONFIG,
    flash_config: FLASH_CONFIG,
};
