#!/usr/bin/env python

import argparse
import subprocess
import gdb
import os
import sys
import shutil

image = gdb.Value.string(gdb.parse_and_eval("$image"))
start_address_hex = gdb.Value.string(gdb.parse_and_eval("$start_address"))
start_address = int(start_address_hex, 16)
key_file = gdb.Value.string(gdb.parse_and_eval("$key"))

# Clean up
if os.path.exists("{}.hex".format(image)):
    os.remove("{}.hex".format(image))
if os.path.exists("{}.bin".format(image)):
    os.remove("{}.bin".format(image))

print("Image {} will be flashed to address {}".format(image, hex(start_address)))

print("Strip debug symbols")
ret = subprocess.call(["llvm-objcopy -O binary {0} {0}.bin".format(image)], shell=True)
if ret != 0:
    sys.exit(ret)

print("Generate image")
ret = subprocess.call(["../../3rdparty/mcuboot/scripts/imgtool.py sign "
                       "-v 1.2.3 "
                       "--align 4 "
                       "--key {1} "
                       "--public-key-format hash "
                       "-S 0x20000 "
                       "-H 256 "
                       "--pad-header "
                       "{0}.bin {0}.img".format(image, key_file)], shell=True)
if ret != 0:
    sys.exit(ret)
print("Convert to hex file. (Arm gdb doesn't support restore....)")
ret = subprocess.call(["llvm-objcopy -I binary -O ihex {0}.img {0}.hex".format(image)], shell=True)
if ret != 0:
    sys.exit(ret)

openocd = subprocess.Popen(["openocd -f board/st_nucleo_f4.cfg"], shell=True)
gdb.execute("target remote :3333")
gdb.execute("load {}.hex {}".format(image, start_address_hex))

openocd.terminate()
openocd.wait()
