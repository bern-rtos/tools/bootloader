#![no_std]
#![no_main]

use stm32f4xx_hal as hal;

use crate::hal::{pac, prelude::*};
use cortex_m_rt::entry;
use core::panic::PanicInfo;

#[entry]
fn main() -> ! {
    let dp = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    // Set up the LED. On the Mini-F4 it's connected to pin PC13.
    let gpioa = dp.GPIOA.split();
    let mut led = gpioa.pa5.into_push_pull_output();

    // Set up the system clock. We want to run at 48MHz for this one.
    let rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr.sysclk(48.mhz()).freeze();

    // Create a delay abstraction based on general-pupose 32-bit timer TIM5
    let mut delay = hal::delay::Delay::tim5(dp.TIM5, &clocks);

    loop {
        // On for 1s, off for 1s.
        led.set_high();
        delay.delay_ms(1_000_u32);
        led.set_low();
        delay.delay_ms(1_000_u32);
    }
}

#[inline(never)]
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {} // You might need a compiler fence in here.
}
