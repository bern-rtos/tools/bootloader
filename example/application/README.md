# Image creation
Make sure to install the dependencies for the image creation tool.
```bash
pip3 install --user -r ../../mcuboot/scripts/requirements.txt
```
Then generate a keyfile. Keep this one secret and stored somewhere safe.
```bash
../../3rdparty/mcuboot/scripts/imgtool.py keygen -k key.pem -t rsa-2048
```
Get the public key with
```bash
../../3rdparty/mcuboot/scripts/imgtool.py getpub -k key.pem
```
Create and flash the image with
```bash
arm-none-eabi-gdb -batch -ex='set $image="../target/thumbv7em-none-eabihf/debug/application"' -ex='set $start_address="0x08020000"' -ex='set $key="./key_rsa.pem"' -x "flash.py"
```
