#![no_std]

pub use rtt_target::{rtt_init_print, rprintln};

pub use core::ptr::slice_from_raw_parts;
pub use mcuboot::{
    mcuboot_flash_map_backend, mcuboot_watchdog, mcuboot_logger, BootRsp, Bootloader, FlashArea, FlashBackend, FlashBackendError,
    FlashSector, log::Log,
};

// Setup of the logger framework.
// This is necessary to link the logger functions to the bootloader if the logging feature is
// enabled
#[mcuboot_logger]
struct Logger {}

impl Log for Logger {
    fn error(err: &str) {
        rprintln!("Error: {}", err);
    }

    fn debug(dbg: &str) {
        rprintln!("Debug: {}", dbg);
    }

    fn warn(warn: &str) {
        rprintln!("Warning: {}", warn);
    }

    fn info(info: &str) {
        rprintln!("Info: {}", info);
    }
}

// Setup of the flash map backend.
// This is required no matter what to run the bootloader.
#[mcuboot_flash_map_backend]
struct FlashDriver {
    flash_areas: [FlashArea; 3],
}

const DEVICE_ID: u8 = 0;
const BOOTLOADER_SIZE: usize = 128 * 1024;
const BOOTLOADER_OFFSET: usize = 0x08000000;
const PRIMARY_SIZE: usize = 128 * 1024;
const PRIMARY_OFFSET: usize = BOOTLOADER_OFFSET + BOOTLOADER_SIZE;
const SECONDARY_SIZE: usize = 128 * 1024;
const SECONDARY_OFFSET: usize = PRIMARY_OFFSET + PRIMARY_SIZE;
const BOOTLOADER_IMAGE_ID: u8 = 0;
const PRIMARY_IMAGE_ID: u8 = 1;
const SECONDARY_IMAGE_ID: u8 = 2;
const FLASH_SECTOR_SIZE: usize = 128 * 1024;

static FLASH_DRIVER: FlashDriver = FlashDriver::new();

impl FlashDriver {
    pub const fn new() -> FlashDriver {
        let bootloader = FlashArea::new(BOOTLOADER_IMAGE_ID, DEVICE_ID, 0, 0, BOOTLOADER_SIZE);
        let primary = FlashArea::new(PRIMARY_IMAGE_ID, DEVICE_ID, 0, PRIMARY_OFFSET, PRIMARY_SIZE);
        let secondary = FlashArea::new(
            SECONDARY_IMAGE_ID,
            DEVICE_ID,
            0,
            SECONDARY_OFFSET,
            SECONDARY_SIZE,
        );
        FlashDriver {
            flash_areas: [bootloader, primary, secondary],
        }
    }

    pub fn get_flash_area(&self, id: u8) -> Result<&FlashArea, FlashBackendError> {
        (&self.flash_areas)
            .into_iter()
            .find(|area: &&FlashArea| id == area.get_id())
            .ok_or(FlashBackendError::Generic)
    }
}

impl<'a> FlashBackend<'a> for FlashDriver {
    fn flash_area_open(id: u8) -> Result<&'static FlashArea, FlashBackendError> {
        let a = FLASH_DRIVER.get_flash_area(id)?;
        Ok(a)
    }

    fn flash_area_close(_fa: FlashArea) {}

    fn flash_area_read(
        fa: &FlashArea,
        off: usize,
        dst: &mut [u8],
    ) -> Result<(), FlashBackendError> {
        unsafe {
            let start = (fa.get_off() + off as usize) as *const u8;
            let data = slice_from_raw_parts(start, dst.len());
            dst.clone_from_slice(&*data);
        }
        Ok(())
    }

    fn flash_area_write(fa: &FlashArea, off: usize, src: &[u8]) -> Result<(), FlashBackendError> {
        unsafe {
            let start = (fa.get_off() + off) as *mut u8;
            for i in 0..src.len() {
                let offset = i as isize;
                *(start.offset(offset)) = src[i];
            }
        }
        Ok(())
    }

    fn flash_area_erase(fa: &FlashArea, off: usize, len: usize) -> Result<(), FlashBackendError> {
        unsafe {
            let start = (fa.get_off() + off) as *mut u8;
            for i in 0..len {
                let offset = i as isize;
                *(start.offset(offset)) = Self::flash_area_erased_val(fa);
            }
        }
        Ok(())
    }

    fn flash_area_align(_fa: &FlashArea) -> usize {
        return 4;
    }

    fn flash_area_erased_val(_fa: &FlashArea) -> u8 {
        return 0xFF;
    }

    fn flash_area_get_sectors(
        fa_id: u8,
        flash_sectors: &mut [FlashSector],
    ) -> Result<usize, FlashBackendError> {
        let area = FLASH_DRIVER.get_flash_area(fa_id)?;
        let mut count: usize = 0;
        match fa_id {
            // The layout of the flash where the bootloader is is more complicated.
            // See STM32F11xC/E page 44
            BOOTLOADER_IMAGE_ID => {
                for i in 0..4 {
                    flash_sectors[count] = FlashSector::new(16 * 1024, i);
                    count += 1;
                }
                flash_sectors[count] = FlashSector::new(64 * 1024, 4);
                count += 1;
            },
            PRIMARY_IMAGE_ID | SECONDARY_IMAGE_ID => {
                let size = area.get_size();
                for i in (0..size).step_by(FLASH_SECTOR_SIZE) {
                    flash_sectors[count] = FlashSector::new(FLASH_SECTOR_SIZE, i);
                    count += 1;
                }
            }
            _ => {}
        }

        Ok(count)
    }

    fn flash_area_id_from_multi_image_slot(
        image_index: u32,
        slot: u32,
    ) -> Result<u32, FlashBackendError> {
        match slot {
            0 => {
                return Ok((MCUBOOT_CONFIG.flash_config.flash_area_image_primary)(image_index));
            }
            1 => {
                return Ok((MCUBOOT_CONFIG.flash_config.flash_area_image_secondary)(image_index));
            }
            _ => Err(FlashBackendError::Generic),
        }
    }

    fn flash_area_id_from_image_slot(slot: u32) -> Result<u32, FlashBackendError> {
        Self::flash_area_id_from_multi_image_slot(0, slot)
    }
}
