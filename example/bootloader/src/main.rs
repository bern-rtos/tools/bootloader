#![no_std]
#![no_main]
#![feature(alloc_error_handler)]

extern crate alloc;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

use core::alloc::Layout;
use core::panic::PanicInfo;
use stm32f4xx_hal as hal;
use crate::hal::{pac, prelude::*};
use cortex_m_rt::entry;
use example::*;
use stm32f4xx_hal::gpio::GpioExt;
use stm32f4xx_hal::rcc::RccExt;
use core::arch::asm;
use alloc_cortex_m::CortexMHeap;

#[mcuboot_watchdog]
fn kick_watchdog() {
    // Kick the lazy watch dog
}

fn jump(rsp: BootRsp) -> ! {
    unsafe {
        let base: *const u8 = 0x0 as *const u8;
        let image_offset = rsp.get_image_off() as isize;
        let header_size = rsp.get_hdr().get_hdr_size() as isize;
        let offset = image_offset + header_size;
        let vector_table: *const u32 = base.offset(offset) as *const u32;

        let app_sp = *vector_table.offset(0);
        let app_start = *vector_table.offset(1);

        let core = pac::CorePeripherals::take().unwrap();
        let vector_table_abs = vector_table as u32;
        core.SCB.vtor.write(vector_table_abs);

        asm!(
            "MSR msp, {}",
            "BLX {}",
            in(reg) app_sp,
            in(reg) app_start,
            options(noreturn)
        )

    }
}

#[entry]
fn main() -> ! {

    rtt_init_print!();
    rprintln!("Bootloader started");

    {
        use core::mem::MaybeUninit;
        const HEAP_SIZE: usize = 1024;
        static mut HEAP: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
        unsafe { ALLOCATOR.init(HEAP.as_ptr() as usize, HEAP_SIZE) }
    }



    let dp = pac::Peripherals::take().unwrap();
    // Set up the LED. On the Mini-F4 it's connected to pin PC13.
    let gpioa = dp.GPIOA.split();
    let mut led = gpioa.pa5.into_push_pull_output();

    // Set up the system clock. We want to run at 48MHz for this one.
    let rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr.sysclk(48.mhz()).freeze();

    // Create a delay abstraction based on general-pupose 32-bit timer TIM5
    let mut delay = hal::delay::Delay::tim5(dp.TIM5, &clocks);


    let rsp = Bootloader::go();
    if let Ok(bl) = rsp {
        jump(bl);
    }

    loop {
        // On for 1s, off for 1s.
        led.set_high();
        delay.delay_ms(200_u32);
        led.set_low();
        delay.delay_ms(200_u32);
    }
}

#[alloc_error_handler]
fn oom(_: Layout) -> ! {
    loop {}
}

#[inline(never)]
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {} // You might need a compiler fence in here.
}
