# Flash
```bash
openocd -f board/st_nucleo_l4.cfg
```

# GDB
```bash 
arm-none-eabi-gdb
target remote :3333
load target/thumbv7em-none-eabihf/debug/bootloader
```

