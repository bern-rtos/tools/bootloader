#[cfg(feature = "with_std")]
use strum::EnumMessage;

#[cfg(feature = "with_std")]
extern crate strum_macros;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct BootUtilKey {
    key: *const u8,
    len: *const usize,
}

// TODO: This needs proper review. Is this ok?
unsafe impl Sync for BootUtilKey {}

impl BootUtilKey {
    pub const fn new(key: &'static [u8], len: &'static usize) -> Self {
        Self {
            key: key.as_ptr(),
            len: len as *const usize,
        }
    }
}

#[cfg(feature = "mbed-tls")]
#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
pub enum MbedTlsSignature {
    #[cfg_attr(feature = "with_std", strum(message = "#define MCUBOOT_SIGN_RSA"))]
    Rsa,

    #[cfg_attr(feature = "with_std", strum(message = "#define MCUBOOT_SIGN_EC256"))]
    EC256,
}

#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
#[cfg(feature = "tinycrypt")]
pub enum TinycryptSignature {
    #[cfg_attr(feature = "with_std", strum(message = "#define MCUBOOT_SIGN_EC256"))]
    EC256,
}

#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
pub enum Encryption {

    #[cfg(feature = "tinycrypt")]
    #[cfg_attr(
        feature = "with_std",
        strum(message = "#define MCUBOOT_USE_TINYCRYPT 1")
    )]
    TinyCrypt(TinycryptSignature),

    #[cfg(feature = "mbed-tls")]
    #[cfg_attr(
        feature = "with_std",
        strum(message = "#define MCUBOOT_USE_MBED_TLS 1")
    )]
    #[cfg(feature = "mbed-tls")]
    MbedTls(MbedTlsSignature),
}

#[cfg(feature = "with_std")]
impl Encryption {
    pub fn get_signature_message(&self) -> Option<&str> {
        match self {
            #[cfg(feature = "mbed-tls")]
            Encryption::MbedTls(sig) => sig.get_message(),

            #[cfg(feature = "tinycrypt")]
            Encryption::TinyCrypt(sig) => sig.get_message(),
        }
    }
}

#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
pub enum OverwriteOnlyMove {
    #[cfg_attr(
        feature = "with_std",
        strum(message = "#define MCUBOOT_OVERWRITE_ONLY_FAST")
    )]
    NecessarySector,

    #[cfg_attr(feature = "with_std", strum(message = ""))]
    AllSectors,
}

#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
pub enum UpgradeMode {
    #[cfg_attr(
        feature = "with_std",
        strum(message = "#define MCUBOOT_SWAP_USING_MOVE 1")
    )]
    SwapMove,
    #[cfg_attr(
        feature = "with_std",
        strum(message = "#define MCUBOOT_OVERWRITE_ONLY")
    )]
    OverwriteOnly(OverwriteOnlyMove),
}

#[cfg(feature = "with_std")]
impl UpgradeMode {
    pub fn get_overwrite_mode_message(&self) -> Option<&str> {
        match self {
            UpgradeMode::SwapMove => Some(""),
            UpgradeMode::OverwriteOnly(overwrite) => overwrite.get_message(),
        }
    }
}

#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
pub enum DirectXip {
    #[cfg_attr(feature = "with_std", strum(message = "#define MCUBOOT_DIRECT_XIP"))]
    Enable,
    #[cfg_attr(feature = "with_std", strum(message = ""))]
    Disable,
}

#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
pub enum RamLoad {
    #[cfg_attr(feature = "with_std", strum(message = "#define MCUBOOT_RAM_LOAD"))]
    Enable,
    #[cfg_attr(feature = "with_std", strum(message = ""))]
    Disable,
}

#[cfg_attr(feature = "with_std", derive(EnumMessage))]
#[derive(Debug)]
pub enum ValidatePrimarySlot {
    #[cfg_attr(
        feature = "with_std",
        strum(message = "#define MCUBOOT_VALIDATE_PRIMARY_SLOT")
    )]
    Enable,
    #[cfg_attr(feature = "with_std", strum(message = ""))]
    Disable,
}

pub struct GeneralConfig<const NUM_OF_KEYS: usize> {
    pub encryption: Encryption,
    pub upgrade_mode: UpgradeMode,
    pub direct_xip: DirectXip,
    pub ram_load: RamLoad,
    pub validate_primary_slot: ValidatePrimarySlot,
    pub keys: [BootUtilKey; NUM_OF_KEYS],
    pub max_image_sectors: usize,
    pub number_of_images: usize,
}

#[cfg(feature = "with_std")]
impl<const NUM_OF_KEYS: usize> GeneralConfig<NUM_OF_KEYS> {
    pub fn get_max_image_sectors_message(&self) -> String {
        format!("#define MCUBOOT_MAX_IMG_SECTORS {}", self.max_image_sectors)
    }
    pub fn get_number_of_images_message(&self) -> String {
        format!("#define MCUBOOT_IMAGE_NUMBER {}", self.number_of_images)
    }
}
