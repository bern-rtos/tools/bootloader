pub struct SysflashConfig {
    pub device_internal_flash: u32,
    pub slot_does_not_exist: u32,
    pub flash_area_image_scratch: u32,
    pub flash_area_bootloader: u32,
    pub flash_area_image_primary: fn(u32) -> u32,
    pub flash_area_image_secondary: fn(u32) -> u32,
}
