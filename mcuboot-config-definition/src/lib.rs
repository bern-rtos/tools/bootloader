#![cfg_attr(not(feature = "with_std"), no_std)]

#[cfg(feature = "with_std")]
extern crate strum;

#[cfg(feature = "with_std")]
pub mod generator;

mod general;
mod sysflash;

pub use general::*;
pub use sysflash::*;

pub struct McubootConfig<const NUM_OF_KEYS: usize> {
    pub general_config: GeneralConfig<NUM_OF_KEYS>,
    pub flash_config: SysflashConfig,
}
