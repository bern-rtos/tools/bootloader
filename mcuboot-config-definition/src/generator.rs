use crate::{GeneralConfig, SysflashConfig};
use strum::EnumMessage;

pub fn general_config<const NUM_OF_KEYS: usize>(
    config: &GeneralConfig<NUM_OF_KEYS>,
) -> Vec<String> {
    vec![
        "#ifndef RUST_MCUBOOT_GENERAL_CONFIG".to_string(),
        "#define RUST_MCUBOOT_GENERAL_CONFIG".to_string(),
        config.encryption.get_message().unwrap().to_string(),
        config
            .encryption
            .get_signature_message()
            .unwrap()
            .to_string(),
        config.upgrade_mode.get_message().unwrap().to_string(),
        config.direct_xip.get_message().unwrap().to_string(),
        config.ram_load.get_message().unwrap().to_string(),
        config
            .validate_primary_slot
            .get_message()
            .unwrap()
            .to_string(),
        config.get_max_image_sectors_message(),
        config.get_number_of_images_message(),

        // Will only be added if the feature is set
        #[cfg(feature = "logging")]
        "#define MCUBOOT_HAVE_LOGGING 1".to_string(),
        "#define MCUBOOT_FIH_PROFILE_OFF".to_string(),
        "#define MCUBOOT_USE_FLASH_AREA_GET_SECTORS".to_string(), // Only get sectors is currently supported
        get_watchdog_config(),
        "#define MCUBOOT_SIGN_RSA_LEN 2048".to_string(),          // TODO: Temporary placeholder
        "#define MCUBOOT_HAVE_ASSERT_H".to_string(), // Since we already have the option, we want rust to handle the asserts
        "#endif".to_string(),
    ]
}

cfg_if::cfg_if! {
    if #[cfg(feature = "watchdog")] {
        fn get_watchdog_config() -> String {
            "#include <rust_support/watchdog.h>\n
            #define MCUBOOT_WATCHDOG_FEED() __rust_mcuboot_watchdog_kick()".to_string()
        }
    } else {
        fn get_watchdog_config() -> String {
            "#define MCUBOOT_WATCHDOG_FEED()".to_string()
        }
    }
}

pub fn sysflash_config(_config: &SysflashConfig) -> Vec<String> {
    vec![
        "#ifndef RUST_MCUBOOT_SYSFLASH_CONFIG".to_string(),
        "#define RUST_MCUBOOT_SYSFLASH_CONFIG".to_string(),
        "#include <rust_support/flash.h>".to_string(),
        "#endif".to_string(),
    ]
}
