use crate::mcuboot_bindings::{FlashArea, FlashSector};
use crate::MCUBOOT_CONFIG;

pub enum FlashBackendError {
    Generic,
}

pub trait FlashBackend<'a> {
    fn flash_area_open(id: u8) -> Result<&'static FlashArea, FlashBackendError>;
    fn flash_area_close(fa: FlashArea);
    fn flash_area_read(fa: &FlashArea, off: usize, dst: &mut [u8])
        -> Result<(), FlashBackendError>;
    fn flash_area_write(fa: &FlashArea, off: usize, src: &[u8]) -> Result<(), FlashBackendError>;
    fn flash_area_erase(fa: &FlashArea, off: usize, len: usize) -> Result<(), FlashBackendError>;
    fn flash_area_align(fa: &FlashArea) -> usize;
    fn flash_area_erased_val(fa: &FlashArea) -> u8;
    fn flash_area_get_sectors(
        fa_id: u8,
        sectors: &mut [FlashSector],
    ) -> Result<usize, FlashBackendError>;
    fn flash_area_id_from_multi_image_slot(
        image_index: u32,
        slot: u32,
    ) -> Result<u32, FlashBackendError>;
    fn flash_area_id_from_image_slot(slot: u32) -> Result<u32, FlashBackendError>;
}

#[no_mangle]
extern "C" fn flash_area_get_id(fa: *const FlashArea) -> u8 {
    return unsafe { (&*fa).get_id() };
}

#[no_mangle]
extern "C" fn flash_area_get_device_id(fa: *const FlashArea) -> u8 {
    return unsafe { (&*fa).get_device_id() };
}

#[no_mangle]
extern "C" fn flash_area_get_off(fa: *const FlashArea) -> usize {
    return unsafe { (&*fa).get_off() };
}

#[no_mangle]
extern "C" fn flash_area_get_size(fa: *const FlashArea) -> usize {
    return unsafe { (&*fa).get_size() };
}

#[no_mangle]
extern "C" fn flash_sector_get_off(fs: *const FlashSector) -> usize {
    return unsafe { (&*fs).get_off() };
}

#[no_mangle]
extern "C" fn flash_sector_get_size(fs: *const FlashSector) -> usize {
    return unsafe { (&*fs).get_size() };
}

#[no_mangle]
extern "C" fn rust_mcuboot_get_sysflash_primary(id: u32) -> u32 {
    return (MCUBOOT_CONFIG.flash_config.flash_area_image_primary)(id);
}

#[no_mangle]
extern "C" fn rust_mcuboot_get_sysflash_secondary(id: u32) -> u32 {
    return (MCUBOOT_CONFIG.flash_config.flash_area_image_secondary)(id);
}
