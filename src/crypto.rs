use crate::MCUBOOT_CONFIG;
use mcuboot_config::{BootUtilKey, GeneralConfig};

const NUMBER_OF_KEYS: usize = get_key_len(&MCUBOOT_CONFIG.general_config);

#[no_mangle]
pub static bootutil_keys: [BootUtilKey; NUMBER_OF_KEYS] =
    generate_keys(&MCUBOOT_CONFIG.general_config);

const fn generate_keys<const NUM_OF_KEYS: usize>(
    general: &GeneralConfig<NUM_OF_KEYS>,
) -> [BootUtilKey; NUM_OF_KEYS] {
    general.keys
}

const fn get_key_len<const NUM_OF_KEYS: usize>(_general: &GeneralConfig<NUM_OF_KEYS>) -> usize {
    NUM_OF_KEYS
}

#[no_mangle]
pub static bootutil_key_cnt: usize = get_key_len(&MCUBOOT_CONFIG.general_config);
