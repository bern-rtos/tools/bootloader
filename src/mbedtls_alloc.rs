#[cfg(feature = "alloc")]
mod mbedtls_alloc {
    use core::alloc::{Layout};
    use alloc::alloc::{alloc, dealloc};
    use alloc::vec::Vec;

    // A hash map is unfortunately not available in no_std, therefore for the moment we are using
    // a vector where we have to find the entry ourselves
    // TODO: We could potentially use "hashbrown" or another no_std hash map here.
    static mut LAYOUT_MAP: Vec<(u32, Layout)> = Vec::new();

    #[no_mangle]
    extern "C" fn __rust_mcuboot_mbedtls_calloc(n: usize, len: usize) -> *mut u8 {
        unsafe {
            let layout = Layout::from_size_align_unchecked(n * len, 8);
            let ptr = alloc(layout.clone());
            LAYOUT_MAP.push((ptr as u32, layout));
            return ptr;
        }
    }

    #[no_mangle]
    extern "C" fn __rust_mcuboot_mbedtls_free(ptr: *mut u8) {
        unsafe {
            let ptr_as_key = ptr as u32;
            let position = LAYOUT_MAP.iter().position(|(key, _)| {
                return *key == ptr_as_key;
            });

            if let Some(pos) = position {
                let (_, layout) = LAYOUT_MAP.remove(pos);
                dealloc(ptr, layout);
            }
        }
    }
}
