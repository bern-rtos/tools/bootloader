use crate::{mcuboot_bindings, FlashArea, BootSwapState};

pub enum BootSwapType {
    None,
    Test,
    Perm,
    Revert,
}

pub enum BootSwapTypeError {
    Fail,
    Panic
}

fn wrap_return_boot_swap_type(boot_type: i32) -> Result<BootSwapType, BootSwapTypeError> {
    match boot_type {
        mcuboot_bindings::BOOT_SWAP_TYPE_NONE => Ok(BootSwapType::None),
        mcuboot_bindings::BOOT_SWAP_TYPE_TEST => Ok(BootSwapType::Test),
        mcuboot_bindings::BOOT_SWAP_TYPE_PERM => Ok(BootSwapType::Perm),
        mcuboot_bindings::BOOT_SWAP_TYPE_REVERT => Ok(BootSwapType::Revert),
        mcuboot_bindings::BOOT_SWAP_TYPE_FAIL => Err(BootSwapTypeError::Fail),
        mcuboot_bindings::BOOT_SWAP_TYPE_PANIC => Err(BootSwapTypeError::Panic),
        _ => Err(BootSwapTypeError::Panic),
    }
}

pub fn boot_swap_type_multi(image_index: i32) -> Result<BootSwapType, BootSwapTypeError> {
    let ret = unsafe {mcuboot_bindings::boot_swap_type_multi(image_index)};
    wrap_return_boot_swap_type(ret)
}

pub fn boot_swap_type() -> Result<BootSwapType, BootSwapTypeError> {
    let ret = unsafe {mcuboot_bindings::boot_swap_type()};
    wrap_return_boot_swap_type(ret)
}

pub enum BootUtilError {
    BadMagic,
    EFlash,
    BadVect,
    Unkown,
}

pub enum SetPendingType {
    Once,
    ForEver,
}

fn match_boot_util_error(ret: i32) -> Result<i32, BootUtilError> {
    match ret {
        0 => Ok(ret),
        mcuboot_bindings::BOOT_MAGIC_BAD => Err(BootUtilError::BadMagic),
        mcuboot_bindings::BOOT_EFLASH => Err(BootUtilError::EFlash),
        mcuboot_bindings::BOOT_EBADVECT => Err(BootUtilError::BadVect),
        _ => Err(BootUtilError::Unkown),
    }
}

pub fn boot_set_pending_multi(image_index: i32, permanent: SetPendingType) -> Result<i32, BootUtilError> {
    let permanent_index = match permanent {
        SetPendingType::Once => 0,
        SetPendingType::ForEver => 1,
    };

    let ret = unsafe {mcuboot_bindings::boot_set_pending_multi(image_index, permanent_index)};
    return match_boot_util_error(ret);
}

pub fn boot_set_confirmed() -> Result<i32, BootUtilError> {
    let ret = unsafe {mcuboot_bindings::boot_set_confirmed()};
    return match_boot_util_error(ret);
}

pub fn boot_swap_info_off(fap: &FlashArea) -> Result<i32, BootUtilError> {
    let fap_as_ptr = fap as *const FlashArea;
    let ret = unsafe {mcuboot_bindings::boot_swap_info_off(fap_as_ptr)};
    return match_boot_util_error(ret);
}

pub fn boot_read_image_ok(fap: *const FlashArea, image_ok: &mut u8) -> Result<i32, BootUtilError> {
    let fap_as_ptr = fap as *const FlashArea;
    let image_ok_as_ptr = image_ok as *mut u8;
    let ret = unsafe {mcuboot_bindings::boot_read_image_ok(fap_as_ptr, image_ok_as_ptr)};
    return match_boot_util_error(ret);
}

pub fn boot_read_swap_state_by_id(flash_area_id: i32, state: &mut BootSwapState) -> Result<i32, BootUtilError> {
    let state_as_ptr = state as *mut BootSwapState;
    let ret = unsafe {mcuboot_bindings::boot_read_swap_state_by_id(flash_area_id, state_as_ptr)};
    return match_boot_util_error(ret);
}

pub fn boot_read_swap_state(fap: &FlashArea, state: &mut BootSwapState) -> Result<i32, BootUtilError> {
    let state_as_ptr = state as *mut BootSwapState;
    let fap_as_ptr = fap as *const FlashArea;
    let ret = unsafe {mcuboot_bindings::boot_read_swap_state(fap_as_ptr, state_as_ptr)};
    return match_boot_util_error(ret);
}
