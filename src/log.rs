
pub trait Log {
    fn error(err: &str);
    fn debug(dbg: &str);
    fn warn(warn: &str);
    fn info(info: &str);
}
