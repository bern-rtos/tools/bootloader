#![no_std]

#[cfg(feature = "alloc")]
extern crate alloc;

extern crate drogue_ffi_compat;

pub mod assert;
pub mod crypto;
pub mod flash;
mod mcuboot_bindings;
pub mod utils;
pub mod log;
pub mod mbedtls_alloc;

pub use mcuboot_config::MCUBOOT_CONFIG;
pub struct Bootloader {}

pub enum BootloaderGoError {
    NonMatching,
    Error,
}

impl Bootloader {
    pub fn go() -> Result<BootRsp, BootloaderGoError> {
        let mut boot_rst = BootRsp::default();
        let rsp_ptr = (&mut boot_rst) as *mut BootRsp;

        unsafe {
            let res = boot_go(rsp_ptr);
            let matched = match res {
                0 => Ok(boot_rst),
                -1 => Err(BootloaderGoError::NonMatching),
                _ => Err(BootloaderGoError::Error),
            };

            return matched;
        }
    }
}

pub use cty;
pub use flash::*;
pub use mcuboot_bindings::*;
pub use mcuboot_proc::mcuboot_flash_map_backend;

#[cfg(feature = "logging")]
pub use mcuboot_proc::mcuboot_logger;

#[cfg(feature = "watchdog")]
pub use mcuboot_proc::mcuboot_watchdog;
