#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]
#![allow(clippy::const_static_lifetime)]
#![allow(clippy::unreadable_literal)]
#![allow(clippy::cyclomatic_complexity)]
#![allow(clippy::useless_transmute)]
#![allow(unused)]

#[cfg(feature = "bootloader")]
pub use bootloader::*;


pub const BOOT_SWAP_TYPE_NONE: i32 = 1;

/**
 * Swap to the secondary slot.
 * Absent a confirm command, revert back on next boot.
 */
pub const BOOT_SWAP_TYPE_TEST: i32 = 2;

/**
 * Swap to the secondary slot,
 * and permanently switch to booting its contents.
 */
pub const BOOT_SWAP_TYPE_PERM: i32 = 3;

/** Swap back to alternate slot.  A confirm changes this state to NONE. */
pub const BOOT_SWAP_TYPE_REVERT: i32 = 4;

/** Swap failed because image to be run is not valid */
pub const BOOT_SWAP_TYPE_FAIL: i32 = 5;

/** Swapping encountered an unrecoverable error */
pub const BOOT_SWAP_TYPE_PANIC: i32 = 0xff;

pub const BOOT_MAGIC_GOOD: i32 = 1;
pub const BOOT_MAGIC_BAD: i32 = 2;
pub const BOOT_MAGIC_UNSET: i32 = 3;
pub const BOOT_MAGIC_ANY: i32 = 4;  /* NOTE: control only, not dependent on sector */
pub const BOOT_MAGIC_NOTGOOD: i32 = 5;  /* NOTE: control only, not dependent on sector */

pub const BOOT_FLAG_SET: i32 = 1;
pub const BOOT_FLAG_BAD: i32 = 2;
pub const BOOT_FLAG_UNSET: i32 = 3;
pub const BOOT_FLAG_ANY: i32 = 4;  /* NOTE: control only, not dependent on sector */

pub const BOOT_EFLASH: i32 = 1;
pub const BOOT_EFILE: i32 = 2;
pub const BOOT_EBADIMAGE: i32 = 3;
pub const BOOT_EBADVECT: i32 = 4;
pub const BOOT_EBADSTATUS: i32 = 5;
pub const BOOT_ENOMEM: i32 = 6;
pub const BOOT_EBADARGS: i32 = 7;
pub const BOOT_EBADVERSION: i32 = 8;
pub const BOOT_EFLASH_SEC: i32 = 9;

#[repr(C)]
#[derive(Debug, Default, Copy, Clone)]
pub struct BootSwapState {
    pub magic: u8,
    pub swap_type: u8,
    pub copy_done: u8,
    pub image_ok: u8,
    pub image_num: u8,
}

extern "C" {
    pub fn boot_swap_type_multi(image_index: cty::c_int) -> cty::c_int;
}
extern "C" {
    pub fn boot_swap_type() -> cty::c_int;
}
extern "C" {
    pub fn boot_set_pending_multi(image_index: cty::c_int, permanent: cty::c_int) -> cty::c_int;
}
extern "C" {
    pub fn boot_set_pending(permanent: cty::c_int) -> cty::c_int;
}
extern "C" {
    pub fn boot_set_confirmed_multi(image_index: cty::c_int) -> cty::c_int;
}
extern "C" {
    pub fn boot_set_confirmed() -> cty::c_int;
}
extern "C" {
    pub fn boot_swap_info_off(fap: *const FlashArea) -> i32;
}
extern "C" {
    pub fn boot_read_image_ok(fap: *const FlashArea, image_ok: *mut u8) -> cty::c_int;
}
extern "C" {
    pub fn boot_read_swap_state_by_id(
        flash_area_id: cty::c_int,
        state: *mut BootSwapState,
    ) -> cty::c_int;
}
extern "C" {
    pub fn boot_read_swap_state(fa: *const FlashArea, state: *mut BootSwapState) -> cty::c_int;
}

#[cfg(feature = "bootloader")]
pub mod bootloader {
    use crate::fih_int;

    #[repr(C)]
    #[derive(Debug, Copy, Clone)]
    pub struct ImageVersion {
        major: u8,
        minor: u8,
        revision: u16,
        build_num: u32,
    }

    impl ImageVersion {
        pub fn get_major(&self) -> u8 {
            self.major
        }

        pub fn get_minor(&self) -> u8 {
            self.minor
        }

        pub fn get_revision(&self) -> u16 {
            self.revision
        }

        pub fn get_build_num(&self) -> u32 {
            self.build_num
        }
    }

    #[repr(C)]
    #[derive(Debug, Copy, Clone)]
    pub struct ImageHeader {
        magic: u32,
        load_addr: u32,
        hdr_size: u16,
        protect_tlv_size: u16,
        img_size: u32,
        flags: u32,
        ver: ImageVersion,
        _pad1: u32,
    }

    impl ImageHeader {
        pub fn get_magic(&self) -> u32 {
            self.magic
        }
        pub fn get_load_addr(&self) -> u32 {
            self.load_addr
        }
        pub fn get_hdr_size(&self) -> usize {
            self.hdr_size as usize
        }
        pub fn get_protect_tlv_size(&self) -> usize {
            self.protect_tlv_size as usize
        }
        pub fn get_img_size(&self) -> usize {
            self.img_size as usize
        }
        pub fn get_flags(&self) -> u32 {
            self.flags
        }
        pub fn get_ver(&self) -> ImageVersion {
            self.ver
        }
    }

    #[repr(C)]
    #[derive(Debug, Copy, Clone)]
    pub struct BootRsp {
        hdr: *const ImageHeader,
        flash_dev_id: u8,
        image_off: usize,
    }

    impl BootRsp {
        pub fn new(hdr: *const ImageHeader, flash_dev_id: u8, image_off: usize) -> Self {
            Self {
                hdr,
                flash_dev_id,
                image_off,
            }
        }

        pub fn get_hdr_as_prt(&self) -> *const ImageHeader {
            self.hdr
        }

        pub fn get_hdr(&self) -> &ImageHeader {
            unsafe { &*self.hdr }
        }

        pub fn get_flash_dev_id(&self) -> u8 {
            self.flash_dev_id
        }

        pub fn get_image_off(&self) -> usize {
            self.image_off
        }
    }
    impl Default for BootRsp {
        fn default() -> Self {
            let mut s = ::core::mem::MaybeUninit::<Self>::uninit();
            unsafe {
                ::core::ptr::write_bytes(s.as_mut_ptr(), 0, 1);
                s.assume_init()
            }
        }
    }
    #[repr(C)]
    #[derive(Debug, Default, Copy, Clone)]
    pub struct image_trailer {
        pub swap_type: u8,
        pub pad1: [u8; 7usize],
        pub copy_done: u8,
        pub pad2: [u8; 7usize],
        pub image_ok: u8,
        pub pad3: [u8; 7usize],
        pub magic: [u8; 16usize],
    }
    extern "C" {
        pub fn boot_go(rsp: *mut BootRsp) -> fih_int;
    }
    #[repr(C)]
    #[derive(Debug, Copy, Clone)]
    pub struct boot_loader_state {
        _unused: [u8; 0],
    }
    extern "C" {
        pub fn context_boot_go(state: *mut boot_loader_state, rsp: *mut BootRsp) -> fih_int;
    }
    extern "C" {
        pub fn split_go(
            loader_slot: cty::c_int,
            split_slot: cty::c_int,
            entry: *mut *mut cty::c_void,
        ) -> fih_int;
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct FlashArea {
    id: u8,
    device_id: u8,
    pad16: u16,
    off: usize,
    size: usize,
}

impl FlashArea {
    pub const fn new(id: u8, device_id: u8, pad16: u16, off: usize, size: usize) -> Self {
        Self {
            id,
            device_id,
            pad16,
            off,
            size,
        }
    }

    pub fn get_id(&self) -> u8 {
        self.id
    }
    pub fn get_device_id(&self) -> u8 {
        self.device_id
    }
    pub fn get_pad16(&self) -> u16 {
        self.pad16
    }
    pub fn get_off(&self) -> usize {
        self.off
    }
    pub fn get_size(&self) -> usize {
        self.size
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone, Default)]
pub struct FlashSector {
    off: usize,
    size: usize,
}

impl FlashSector {
    pub fn new(size: usize, off: usize) -> Self {
        Self { size, off }
    }
    pub fn get_off(&self) -> usize {
        self.off
    }
    pub fn get_size(&self) -> usize {
        self.size
    }
}

extern "C" {
    pub static boot_img_magic: [u32; 4usize];
}
pub type fih_int = cty::c_int;
extern "C" {
    pub static mut FIH_SUCCESS: fih_int;
}
extern "C" {
    pub static mut FIH_FAILURE: fih_int;
}
extern "C" {
    pub fn fih_cfi_get_and_increment() -> fih_int;
}
extern "C" {
    pub fn fih_cfi_validate(saved: fih_int);
}
extern "C" {
    pub fn fih_cfi_decrement();
}
