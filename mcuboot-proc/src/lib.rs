use proc_macro::TokenStream;
use quote::quote;
use syn::{ItemStruct};

#[cfg(feature = "logging")]
#[proc_macro_attribute]
pub fn mcuboot_logger(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let logger_struct: ItemStruct = syn::parse_macro_input!(item as ItemStruct);
    let logger_name = &logger_struct.ident;
    let logger_generics = &logger_struct.generics;

    TokenStream::from(quote! {

        #logger_struct

        #[no_mangle]
        #[link(name = "mcuboot")]
        extern "C" fn __rust_mcuboot_log_err(data: *const u8, len: usize) {
            unsafe {
                let raw = core::slice::from_raw_parts(data, len);
                let res_message = core::str::from_utf8(raw);
                if let Ok(message) = res_message {
                    <#logger_name #logger_generics as Log>::error(message);
                }
            }
        }

        #[no_mangle]
        #[link(name = "mcuboot")]
        extern "C" fn __rust_mcuboot_log_dbg(data: *const u8, len: usize) {
            unsafe {
                let raw = core::slice::from_raw_parts(data, len);
                let res_message = core::str::from_utf8(raw);
                if let Ok(message) = res_message {
                    <#logger_name #logger_generics as Log>::debug(message);
                }
            }
        }

        #[no_mangle]
        #[link(name = "mcuboot")]
        extern "C" fn __rust_mcuboot_log_wrn(data: *const u8, len: usize) {
            unsafe {
                let raw = core::slice::from_raw_parts(data, len);
                let res_message = core::str::from_utf8(raw);
                if let Ok(message) = res_message {
                    <#logger_name #logger_generics as Log>::warn(message);
                }
            }
        }

        #[no_mangle]
        #[link(name = "mcuboot")]
        extern "C" fn __rust_mcuboot_log_inf(data: *const u8, len: usize) {
            unsafe {
                let raw = core::slice::from_raw_parts(data, len);
                let res_message = core::str::from_utf8(raw);
                if let Ok(message) = res_message {
                    <#logger_name #logger_generics as Log>::info(message);
                }
            }
        }
    })
}

#[cfg(feature = "watchdog")]
#[proc_macro_attribute]
pub fn mcuboot_watchdog(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let watchdog_function: syn::ItemFn = syn::parse_macro_input!(item as syn::ItemFn);
    let watchdog_function_ident: &proc_macro2::Ident = &watchdog_function.sig.ident;
    TokenStream::from(quote!{
        #watchdog_function
        #[no_mangle]
        #[link(name = "mcuboot")]
        extern "C" fn __rust_mcuboot_watchdog_kick() {
            #watchdog_function_ident();
        }
    })
}

#[proc_macro_attribute]
pub fn mcuboot_flash_map_backend(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let flash_map_backend_struct: ItemStruct = syn::parse_macro_input!(item as ItemStruct);
    let flash_map_backend_name = &flash_map_backend_struct.ident;
    let flash_map_backend_generics = &flash_map_backend_struct.generics;

    TokenStream::from(quote! {

        use mcuboot::cty;
        use mcuboot::MCUBOOT_CONFIG;

        #flash_map_backend_struct

        #[no_mangle]
        #[link(name = "mcuboot")]
        extern "C" fn flash_area_open(id: u8, area_outp: *mut *const FlashArea) -> cty::c_int {
            if let Ok(flash_area) = <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_open(id) {
                unsafe {
                    *area_outp = flash_area
                }
                0
            } else {
                -1
            }
        }

        #[no_mangle]
        #[link(name = "mcuboot")]
        extern "C" fn flash_area_close(fa: *const FlashArea) {
            unsafe {
                <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_close(*fa);
            }
        }

        #[no_mangle]
        extern "C" fn flash_area_read(fa: *const FlashArea, off: u32, dst: *mut cty::c_void, len: u32) -> cty::c_int {
            let res = unsafe {
                let cast_dst = dst as *mut u8;
                let slice = core::slice::from_raw_parts_mut(cast_dst, len as usize);
                <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_read(&*fa, off as usize, slice)
            };
            if let Ok(flash_area) = res {
                0
            } else {
                -1
            }
        }

        #[no_mangle]
        extern "C" fn flash_area_write( fa: *const FlashArea, off: u32, src: *const cty::c_void, len: u32) -> cty::c_int {
            let res = unsafe {
                let cast_src = src as *const u8;
                let slice = core::slice::from_raw_parts(cast_src, len as usize);
                <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_write(&*fa, off as usize, slice)
            };
            if let Ok(flash_area) = res {
                0
            } else {
                -1
            }
        }

        #[no_mangle]
        extern "C" fn flash_area_erase(fa: *const FlashArea, off: u32, len: u32) -> cty::c_int  {
            let res = unsafe {
                <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_erase(&*fa, off as usize, len as usize)
            };
            if let Ok(flash_area) = res {
                0
            } else {
                -1
            }
        }

        #[no_mangle]
        extern "C" fn flash_area_align(fa: *const FlashArea) -> usize {
            return unsafe { <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_align(&*fa) };
        }

        #[no_mangle]
        extern "C" fn flash_area_erased_val(fa: *const FlashArea) -> u8 {
            return unsafe { <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_erased_val(&*fa) };
        }

        #[no_mangle]
        extern "C" fn flash_area_get_sectors(fa_id: cty::c_int, count: *mut u32, sectors: *mut FlashSector) -> cty::c_int {
            let mut flash_sectors: [FlashSector; MCUBOOT_CONFIG.general_config.max_image_sectors] = [FlashSector::default(); MCUBOOT_CONFIG.general_config.max_image_sectors];
            let res = unsafe {
                <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_get_sectors(fa_id as u8, &mut flash_sectors[..])
            };
            if let Ok(sectors_count) = res {
                unsafe {
                    let dst_sectors = core::slice::from_raw_parts_mut(sectors, sectors_count);
                    *count = sectors_count as u32;
                    for i in 0..sectors_count {
                        dst_sectors[i] = flash_sectors[i].clone();
                    }
                }
                0
            } else {
                -1
            }
        }

        #[no_mangle]
        extern "C" fn flash_area_id_from_multi_image_slot(image_index: cty::c_int, slot: cty::c_int) -> cty::c_int {
            let res = unsafe {
                <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_id_from_multi_image_slot(image_index as u32, slot as u32)
            };
            if let Ok(id) = res {
                id as i32
            } else {
                -1
            }
        }

        #[no_mangle]
        extern "C" fn flash_area_id_from_image_slot(slot: cty::c_int) -> cty::c_int {
            let res = unsafe {
                <#flash_map_backend_name #flash_map_backend_generics as FlashBackend>::flash_area_id_from_image_slot(slot as u32)
            };
            if let Ok(id) = res {
                id as i32
            } else {
                -1
            }
        }
    })
}
