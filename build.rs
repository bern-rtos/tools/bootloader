use mcuboot_config;
use mcuboot_config::MCUBOOT_CONFIG;
use mcuboot_config_definition::generator;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::{env, fs};
use cfg_if::cfg_if;

fn get_mcuboot_out_path() -> PathBuf {
    PathBuf::from(env::var("OUT_DIR").unwrap())
}

fn get_mcuboot_generated_include_path() -> PathBuf {
    get_mcuboot_out_path().join("generated/include/")
}

const fn get_general_config_file_path() -> &'static str {
    return "mcuboot_config/mcuboot_config.h";
}

const fn get_sysflash_config_file_path() -> &'static str {
    return "sysflash/sysflash.h";
}

#[cfg(feature="mbed-tls")]
fn compile_mbedtls() {

    let header_dirs = vec![
        "common/include/os/",
        "common/include/mbedtls_config",
        "3rdparty/mcuboot/ext/mbedtls/include",
        "3rdparty/mcuboot/ext/mbedtls-asn1/include",
        "3rdparty/mcuboot/ext/mbedtls/library",
    ];

    let source_files = vec![
        "common/rust_support/mbedtls_alloc.c",
        "3rdparty/mcuboot/ext/mbedtls-asn1/src/asn1parse.c",
        "3rdparty/mcuboot/ext/mbedtls-asn1/src/platform_util.c",
        "3rdparty/mcuboot/ext/mbedtls/library/sha256.c",
        "3rdparty/mcuboot/ext/mbedtls/library/version.c",
        "3rdparty/mcuboot/ext/mbedtls/library/aes.c",
        "3rdparty/mcuboot/ext/mbedtls/library/ecp.c",
        "3rdparty/mcuboot/ext/mbedtls/library/bignum.c",
        "3rdparty/mcuboot/ext/mbedtls/library/ecdh.c",
        "3rdparty/mcuboot/ext/mbedtls/library/nist_kw.c",
        "3rdparty/mcuboot/ext/mbedtls/library/cmac.c",
        "3rdparty/mcuboot/ext/mbedtls/library/md.c",
        "3rdparty/mcuboot/ext/mbedtls/library/ecdsa.c",
        "3rdparty/mcuboot/ext/mbedtls/library/ecp_curves.c",
        "3rdparty/mcuboot/ext/mbedtls/library/entropy.c",
        "3rdparty/mcuboot/ext/mbedtls/library/oid.c",
        "3rdparty/mcuboot/ext/mbedtls/library/rsa.c",
    ];

    let mut cc = cc::Build::new();

    cc.includes(header_dirs);
    cc.files(source_files.clone());
    for source_file in source_files {
        println!("cargo:rerun-if-changed={}", source_file);
    }
    cc.compile("mbedtls");

    let out = &PathBuf::from(env::var_os("OUT_DIR").unwrap());
    println!("cargo:rustc-link-search={}", out.display());
}

#[cfg(feature="tinycrypt")]
fn compile_tinycrypt() {
    let header_dirs = vec!["3rdparty/mcuboot/ext/tinycrypt/lib/include"];

    let source_files = vec![
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/aes_decrypt.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/aes_encrypt.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/cbc_mode.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/ccm_mode.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/cmac_mode.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/ctr_mode.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/ctr_prng.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/ecc.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/ecc_dh.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/ecc_dsa.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/ecc_platform_specific.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/hmac.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/hmac_prng.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/sha256.c",
        "3rdparty/mcuboot/ext/tinycrypt/lib/source/utils.c",
    ];

    let mut cc = cc::Build::new();

    cc.includes(header_dirs);
    cc.files(source_files.clone());
    cc.warnings(false); // Ignore warnings. Not pretty, but we are not the ones having to clean those up.
    for source_file in source_files {
        println!("cargo:rerun-if-changed={}", source_file);
    }
    cc.compile("tinycrypt");

    let out = &PathBuf::from(env::var_os("OUT_DIR").unwrap());
    println!("cargo:rustc-link-search={}", out.display());
}

fn compile_mcuboot() {
    let header_dirs = vec![
        // Mbed and tinycrypt header must be included to satisfy the compilation
        "3rdparty/mcuboot/ext/mbedtls/include",
        "3rdparty/mcuboot/ext/mbedtls/library",
        "3rdparty/mcuboot/ext/tinycrypt/lib/include",
        "common/include/",
        "common/",
        "3rdparty/mcuboot/boot/bootutil/include",
        "3rdparty/mcuboot/boot/bootutil/include/crypto",
    ];

    let util_source_files = vec![
        "common/rust_support/log_wrapper.c",
        "3rdparty/mcuboot/boot/bootutil/src/bootutil_public.c"
    ];

    let source_files = {
        cfg_if! {
            if #[cfg(feature = "bootloader")] {
                let mut bootloader_source_files = vec![
                    "3rdparty/mcuboot/boot/bootutil/src/boot_record.c",
                    "3rdparty/mcuboot/boot/bootutil/src/boot_record.c",
                    "3rdparty/mcuboot/boot/bootutil/src/bootutil_misc.c",
                    "3rdparty/mcuboot/boot/bootutil/src/caps.c",
                    "3rdparty/mcuboot/boot/bootutil/src/encrypted.c",
                    "3rdparty/mcuboot/boot/bootutil/src/fault_injection_hardening.c",
                    "3rdparty/mcuboot/boot/bootutil/src/fault_injection_hardening_delay_rng_mbedtls.c",
                    "3rdparty/mcuboot/boot/bootutil/src/image_ec.c",
                    "3rdparty/mcuboot/boot/bootutil/src/image_ec256.c",
                    "3rdparty/mcuboot/boot/bootutil/src/image_ed25519.c",
                    "3rdparty/mcuboot/boot/bootutil/src/image_rsa.c",
                    "3rdparty/mcuboot/boot/bootutil/src/image_validate.c",
                    "3rdparty/mcuboot/boot/bootutil/src/loader.c",
                    "3rdparty/mcuboot/boot/bootutil/src/swap_misc.c",
                    "3rdparty/mcuboot/boot/bootutil/src/swap_move.c",
                    "3rdparty/mcuboot/boot/bootutil/src/swap_scratch.c",
                    "3rdparty/mcuboot/boot/bootutil/src/tlv.c",
                ];
                bootloader_source_files.extend(util_source_files);
                bootloader_source_files
            } else {
                util_source_files
            }
        }
    };

    let generated_include_path = get_mcuboot_generated_include_path();

    let mut cc = cc::Build::new();
    cc.warnings(false); // Ignore warnings. Not pretty, but we are not the ones having to clean those up.
    cc.includes(header_dirs.clone());
    cc.include(generated_include_path);
    cc.files(source_files.clone());
    for source_file in source_files {
        println!("cargo:rerun-if-changed={}", source_file);
    }
    cc.compile("mcuboot");
}

fn write_generated_config(conf: Vec<String>, file_path: &str) {
    let out_path = get_mcuboot_generated_include_path().join(file_path);
    fs::remove_file(&out_path).ok();
    let prefix = out_path.as_path().parent().unwrap();
    std::fs::create_dir_all(prefix).unwrap();

    let mut file = File::create(out_path.as_path()).unwrap();

    for c in conf {
        writeln!(&mut file, "{}", c).unwrap();
    }
}

fn main() {
    write_generated_config(
        generator::general_config(&MCUBOOT_CONFIG.general_config),
        get_general_config_file_path(),
    );
    write_generated_config(
        generator::sysflash_config(&MCUBOOT_CONFIG.flash_config),
        get_sysflash_config_file_path(),
    );

    #[cfg(feature="mbed-tls")]
    compile_mbedtls();

    #[cfg(feature="tinycrypt")]
    compile_tinycrypt();

    compile_mcuboot();
}
