#include <stdbool.h>

void rust_mcuboot_assert(bool exp);

#define assert(exp) rust_mcuboot_assert(exp)
