#ifdef MCUBOOT_HAVE_LOGGING
    #if MCUBOOT_HAVE_LOGGING == 1
        #include <rust_support/log_wrapper.h>
        // TODO: Add the declare wrapper
        #define MCUBOOT_LOG_MODULE_DECLARE(...)
        // TODO: Add the register wrapper
        #define MCUBOOT_LOG_MODULE_REGISTER(...)
        #define MCUBOOT_LOG_ERR(_fmt, ...) __rust_mcuboot_internal_log_err(_fmt, ##__VA_ARGS__)
        #define MCUBOOT_LOG_WRN(_fmt, ...) __rust_mcuboot_internal_log_wrn(_fmt, ##__VA_ARGS__)
        #define MCUBOOT_LOG_INF(_fmt, ...) __rust_mcuboot_internal_log_inf(_fmt, ##__VA_ARGS__)
        #define MCUBOOT_LOG_DBG(_fmt, ...) __rust_mcuboot_internal_log_dbg(_fmt, ##__VA_ARGS__)
    #else
        #define MCUBOOT_LOG_MODULE_DECLARE(...)
        #define MCUBOOT_LOG_MODULE_REGISTER(...)
        #define MCUBOOT_LOG_ERR(fmt, ...)
        #define MCUBOOT_LOG_WRN(fmt, ...)
        #define MCUBOOT_LOG_INF(fmt, ...)
        #define MCUBOOT_LOG_DBG(fmt, ...)
    #endif
#else
#define MCUBOOT_LOG_MODULE_DECLARE(...)
#define MCUBOOT_LOG_MODULE_REGISTER(...)
#define MCUBOOT_LOG_ERR(fmt, ...)
#define MCUBOOT_LOG_WRN(fmt, ...)
#define MCUBOOT_LOG_INF(fmt, ...)
#define MCUBOOT_LOG_DBG(fmt, ...)
#endif

