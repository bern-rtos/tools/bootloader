#include "mbedtls_alloc.h"

void* mbedtls_calloc(size_t n, size_t size) {
    return __rust_mcuboot_mbedtls_calloc(n, size);
}

void mbedtls_free(void *ptr) {
    __rust_mcuboot_mbedtls_free(ptr);
}
