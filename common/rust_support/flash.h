#ifndef MCUBOOT_RUST_SUPPORT_FLASH_H
#define MCUBOOT_RUST_SUPPORT_FLASH_H

#include <inttypes.h>

#define FLASH_AREA_IMAGE_PRIMARY(i) rust_mcuboot_get_sysflash_primary(i)
#define FLASH_AREA_IMAGE_SECONDARY(i) rust_mcuboot_get_sysflash_secondary(i)

uint32_t rust_mcuboot_get_sysflash_primary(uint32_t id);
uint32_t rust_mcuboot_get_sysflash_secondary(uint32_t id);

#endif //MCUBOOT_RUST_SUPPORT_FLASH_H
