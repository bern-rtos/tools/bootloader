#ifndef RUST_MCUBOOT_LOG_H
#define RUST_MCUBOOT_LOG_H

// TODO: Should we make this configurable?
#define RUST_MCUBOOT_LOG_BUFFER_SIZE 200lu

#include <stddef.h>

void __rust_mcuboot_log_declare();
void __rust_mcuboot_log_register();
void __rust_mcuboot_log_err(const char * str, size_t len);
void __rust_mcuboot_log_dbg(const char * str, size_t len);
void __rust_mcuboot_log_wrn(const char * str, size_t len);
void __rust_mcuboot_log_inf(const char * str, size_t len);

void __rust_mcuboot_internal_log_err(const char * format, ... );
void __rust_mcuboot_internal_log_wrn(const char * format, ... );
void __rust_mcuboot_internal_log_inf(const char * format, ... );
void __rust_mcuboot_internal_log_dbg(const char * format, ... );

#endif //MCUBOOT_LOG_H
