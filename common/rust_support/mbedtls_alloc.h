#include <mbedtls/config.h>
#include <mbedtls/platform.h>

/**
 * Wrapper function to forward the allocation to rust
 * @param n
 * @param len
 * @return
 */
void* __rust_mcuboot_mbedtls_calloc(size_t n, size_t len);

/**
 * Wrapper function to forward the deallocation to rust
 * @param ptr
 */
void __rust_mcuboot_mbedtls_free(void* ptr);
