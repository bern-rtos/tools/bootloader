#include "log_wrapper.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>

#define FORWARD_TO_LOG(log_function) \
    char buffer[RUST_MCUBOOT_LOG_BUFFER_SIZE]; \
    va_list args; \
    va_start(args, format); \
    int32_t ret = vsnprintf(buffer,RUST_MCUBOOT_LOG_BUFFER_SIZE, format, args); \
    va_end(args); \
    if (ret >= 0) { \
        log_function(buffer, ret); \
    } \

void __rust_mcuboot_internal_log_err(const char * format, ... ) {
    FORWARD_TO_LOG(__rust_mcuboot_log_err)
}

void __rust_mcuboot_internal_log_wrn(const char * format, ... ) {
    FORWARD_TO_LOG(__rust_mcuboot_log_wrn)
}

void __rust_mcuboot_internal_log_inf(const char * format, ... ) {
    FORWARD_TO_LOG(__rust_mcuboot_log_inf)
}

void __rust_mcuboot_internal_log_dbg(const char * format, ... ) {
    FORWARD_TO_LOG(__rust_mcuboot_log_dbg)
}
